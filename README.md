# Grid Planner

A major barrier to successful interconnection of renewable energy (solar/wind/storage) projects is the lack of information on the grid network/conditions and unpredictability of capital and operational costs. To address this problem, the Cost of Interconnection Prediction Algorithm (CIPA) was developed. CIPA is the method that powers the grid planner application and forecasts the interconnection routes and costs for solar PV, wind and energy installations.

The grid planner repository contains the backend CIPA code and the frontend UI. A high level architectural diagram of the process running locally is shown below:

<div align="center">
<img src="grid_planner_architecture.png" width="500" />
</div>

## Requirements and Setup

1. Install NodeJS and npm (used for the frontend)
  * Windows:
    [NodeJS](https://nodejs.org/en/download/) installed.
  * Linux:
    via [Nvm](https://github.com/nvm-sh/nvm)

Make sure both `node --version` and `npm --version` work before proceeding.

2. Install Docker (used for PostgreSQL)
   * Windows
    [Docker for Windows](https://docs.docker.com/desktop/windows/install/)
   * Linux
    [Install Docker Engine](https://docs.docker.com/engine/install/ubuntu/)
      - Post-installation steps on [user privileges](https://docs.docker.com/engine/install/linux-postinstall/)


For the application to work make sure that Docker Desktop or Docker Engine are up and running.
If not, type `sudo systemctl start docker` to start the docker engine.

3. Install `make`

   * Windows

     Install [Chocolatey](https://chocolatey.org/install) package manager. 
     Run `choco install make` from the Powershell.

   * For Linux `make` comes pre-installed.

4. Install and use python version 3.8 or above.

## Running the application

A Makefile is provided to run Grid-Planner.
Type `make` in the root directory.
 
Visit http://localhost:3000 on your browser.

## Key info

- Communication from the frontend to the backend happens at `grid_planner/views.py`
- All search related code lives in `grid_planner/grid_utils.py`
- All database models (tables) live in `grid_planner/models.py`

You can manually re-execute the search for a given project. Ex. project with id 12

```sh
./manage.py execute_search 12
```



