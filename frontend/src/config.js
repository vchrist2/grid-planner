export const API_ENDPOINT =
  process.env.NODE_ENV === "development"
    ? "http://127.0.0.1:8000/grid_planner/"
    : "https://swallow-16515.herokuapp.com/grid_planner/";
export const MAPBOX_API_KEY =
  "pk.eyJ1Ijoia2Vza2VueSIsImEiOiJjanhrZzAxcXMyOWt6M3lzOGh5Nnc2amRpIn0.dsGmc9mCu0slXioNwVyypw";
