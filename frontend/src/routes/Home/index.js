import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

import { ProjectCard } from "../../components/ProjectCard";
import Banner from "../../components/Banner";
import { Button } from "../../components/Buttons";
import { API_ENDPOINT } from "../../config";
import styles from "./Home.module.css";

const Home = () => {
  const [projects, setProjects] = useState();
  const history = useHistory();

  useEffect(() => {
    const fetchProjects = async () => {
      if (!projects) {
        const query = await fetch(`${API_ENDPOINT}project/?format=json`);
        const data = await query.json();
        setProjects(data);
      }
    };
    fetchProjects();
  }, [projects]);

  return (
    <>
      <Banner />
      <div className={styles.root}>
        <h1>Projects</h1>
        <div className={styles.cardWrapper}>
          {projects?.map((project) => (
            <ProjectCard project={project} key={project?.id} />
          ))}
        </div>
        <Button kind="gradient" onClick={() => history.push("/project/new")}>
          Create Project
        </Button>
      </div>
    </>
  );
};

export default Home;
