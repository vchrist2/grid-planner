import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import { API_ENDPOINT } from "../../config";
import { FaRoute } from "react-icons/fa";

import Map from "../../components/Map";
import LoadingPage from "../../components/LoadingPage";
import Banner from "../../components/Banner";
import { BasicProjectForm } from "../../components/BasicProjectForm";
import { SearchConfigForm } from "../../components/SearchConfigForm";
import {
  reverseGeocode,
  swapLatLng,
  calculateDistance,
} from "../../services/map";
import { Button } from "../../components/Buttons";
import { Report } from "../../components/Report";
import styles from "./Project.module.css";

const isValidId = (id) => !!id && !isNaN(id);

const Project = () => {
  const { id } = useParams();
  const [project, setProject] = useState();
  const [start, setStart] = useState();
  const [loading, setLoading] = useState(false);
  const [end, setEnd] = useState();
  const [location, setLocation] = useState();
  const [searching, setSearching] = useState(false);

  useEffect(() => {
    const fetchProject = async () => {
      if (isValidId(id)) {
        setLoading(true);
        const query = await fetch(`${API_ENDPOINT}project/${id}/?format=json`);
        const data = await query.json();
        setProject(data);
        setStart(data?.start);
        setEnd(data?.end);
        setLoading(false);
      }
    };
    fetchProject();
  }, [id]);

  const onStartMarkerDragEnd = async (lat, lng) => {
    setStart([lat, lng]);
    const body = await reverseGeocode(lng, lat);
    setLocation(body);
  };

  const onEndMarkerDragEnd = (lat, lng) => {
    setEnd([lat, lng]);
  };

  if (isValidId(id) && !project) return <LoadingPage />;

  const onSubmit = async () => {
    if (!project) return null;
    if (calculateDistance(swapLatLng(start), swapLatLng(end)) > 10) {
      console.log(calculateDistance(swapLatLng(start), swapLatLng(end)));
      alert(
        "Distances longer than 10km as the bird flies are not currently supported"
      );
      return;
    }
    setSearching(true);

    const response = await fetch(`${API_ENDPOINT}execute_search/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify({ project_id: project.id }),
    });
    let respData = await response.json();
    if (respData?.id) {
      setProject(respData);
      while (respData.searching) {
        await new Promise((resolve) => setTimeout(resolve, 10000));

        const query = await fetch(`${API_ENDPOINT}project/${id}/?format=json`);
        respData = await query.json();
      }
      if (!respData.searching) {
        window.location.reload();
      }
    }

    setSearching(false);
  };

  return (
    <>
      <Banner />

      <div className={styles.root}>
        <div>
          <BasicProjectForm
            project={project}
            setProject={setProject}
            isNew={!isValidId(id)}
            start={start}
            end={end}
            location={location}
            setStart={setStart}
            setEnd={setEnd}
          />

          {project && (
            <SearchConfigForm project={project} setProject={setProject} />
          )}
        </div>
        <div className={styles.rightHalf}>
          {!loading && (
            <Map
              start={swapLatLng(start)}
              end={swapLatLng(end)}
              className={styles.map}
              onStartMarkerDragEnd={onStartMarkerDragEnd}
              onEndMarkerDragEnd={onEndMarkerDragEnd}
              shortestPath={project?.shortest_path}
            />
          )}

          {isValidId(id) &&
            (searching ? (
              <div className={styles.loader}>Loading</div>
            ) : (
              <Button
                kind="gradient"
                className={styles.searchButton}
                onClick={onSubmit}
              >
                <FaRoute className={styles.routeIcon} /> Find Path
              </Button>
            ))}
        </div>
      </div>
      {project && <Report project={project} />}
    </>
  );
};

export default Project;
