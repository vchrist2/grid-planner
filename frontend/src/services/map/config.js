export const GRID_LAYER_INFO = {
  line: {
    source: "transmission_lines",
    sourceLayer: "lines_accepted-0tmcex",
    mapboxUrl: "mapbox://keskeny.4v86qsh4",
    highlightedId: "highlighted-line",
    highlightedShape: "line",
    highlightedShapeWidth: 1.9,
  },
  transformer: {
    source: "transformers",
    sourceLayer: "transformers_accepted-3iv2l1",
    mapboxUrl: "mapbox://keskeny.3o1dqs0a",
    highlightedId: "highlighted-transformers",
    highlightedShape: "circle",
    highlightedShapeWidth: 5,
  },
  switch: {
    source: "switches",
    sourceLayer: "switches_accepted-a0yjwc",
    mapboxUrl: "mapbox://keskeny.6wtgjgs7",
    highlightedId: "highlighted-switches",
    highlightedShape: "circle",
    highlightedShapeWidth: 5,
  },
  compensator: {
    source: "compensators",
    sourceLayer: "compensators_accepted-52oz8f",
    mapboxUrl: "mapbox://keskeny.c7il0uz5",
    highlightedId: "highlighted-compensators",
    highlightedShape: "circle",
    highlightedShapeWidth: 5,
  },
  substation: {
    source: "substation_polygons",
    sourceLayer: "substations_accepted-2xiw2y",
    mapboxUrl: "mapbox://keskeny.dpgrzi79",
    highlightedId: "highlighted-substations",
    highlightedShape: "fill",
  },
  powerProject: {
    source: "power_plant_points",
    sourceLayer: "power_projects-0c76lk",
    mapboxUrl: "mapbox://keskeny.9d17w4ir",
    highlightedId: "highlighted-power-projects",
    highlightedShape: "circle",
    highlightedShapeWidth: 7,
  },
};

export const getPaintProp = (layer, layerInfo) => {
  const mapHighlightColour = "#ffff00";
  const selector =
    layer === "powerProject"
      ? ["==", ["get", "enian_id"], ["feature-state", "id"]]
      : ["boolean", ["feature-state", "clicked"], false];
  const width = layerInfo.highlightedShapeWidth || 1;
  const highlight = ["case", selector, width, 0];
  switch (layerInfo.highlightedShape) {
    case "circle":
      return {
        "circle-radius": highlight,
        "circle-color": mapHighlightColour,
      };
    case "line":
      return {
        "line-width": highlight,
        "line-color": mapHighlightColour,
      };
    case "fill":
      return {
        "fill-opacity": highlight,
        "fill-color": mapHighlightColour,
      };
    default:
      return {};
  }
};

export const powerProjectsLayers = {
  small: "power-projects-small",
  big: "power-projects-big",
};

export const gridObjects = [
  "line",
  "substations",
  "compensator",
  "switch",
  "transformer",
];

export const MAP_RESOURCE_INFO = {
  gridNetwork: {
    layers: ["line", "switch", "transformer", "substation", "compensator"],
  },
  irradiation: {
    layers: ["horizontal_irradiation"],
  },
  windSpeed: {
    layers: ["windcomp"],
  },
};

export const LAYER_TYPES = {
  "power-projects-big": "Power project",
  "power-projects-small": "Power project",
  line: "Transmission line",
  switch: "Switch",
  transformer: "Transformer",
  substation: "Substation",
  compensator: "Compensator",
};

export const mapObjects = Object.keys(LAYER_TYPES);

export const MAP_BASE_INFO = {
  lightGrey: {
    displayName: "Light grey",
    layers: [
      "water-point-label",
      "water-line-label",
      "natural-point-label",
      "natural-line-label",
      "waterway-label",
      "bridge-motorway-trunk-2",
      "bridge-major-link-2",
      "bridge-motorway-trunk-2-case",
      "bridge-major-link-2-case",
      "bridge-rail",
      "bridge-motorway-trunk",
      "bridge-primary-secondary-tertiary",
      "bridge-street-minor",
      "bridge-pedestrian",
      "bridge-major-link",
      "bridge-steps",
      "bridge-path",
      "bridge-construction",
      "bridge-motorway-trunk-case",
      "bridge-major-link-case",
      "bridge-primary-secondary-tertiary-case",
      "bridge-street-minor-case",
      "bridge-street-minor-low",
      "bridge-pedestrian-case",
      "road-rail",
      "road-motorway-trunk",
      "road-primary",
      "road-secondary-tertiary",
      "road-street",
      "road-minor",
      "road-pedestrian",
      "road-major-link",
      "road-steps",
      "road-path",
      "road-construction",
      "road-motorway-trunk-case",
      "road-major-link-case",
      "road-primary-case",
      "road-secondary-tertiary-case",
      "road-street-case",
      "road-minor-case",
      "road-street-low",
      "road-minor-low",
      "road-pedestrian-case",
      "tunnel-motorway-trunk",
      "tunnel-primary-secondary-tertiary",
      "tunnel-street-minor",
      "tunnel-pedestrian",
      "tunnel-major-link",
      "tunnel-steps",
      "tunnel-path",
      "tunnel-construction",
      "tunnel-motorway-trunk-case",
      "tunnel-major-link-case",
      "tunnel-primary-secondary-tertiary-case",
      "tunnel-street-minor-case",
      "tunnel-street-minor-low",
      "building",
      "building-outline",
      "aeroway-line",
      "aeroway-polygon",
      "land-structure-line",
      "land-structure-polygon",
      "hillshade",
      "water",
      "waterway",
      "water-shadow",
      "landuse",
      "national-park",
      "landcover",
      "land",
    ],
  },
  satellite: {
    displayName: "Satellite",
    layers: ["mapbox-satellite"],
  },
};

export const MAP_LEGEND = {
  Biomass: "#317c3f",
  Petcoke: "#980d0d",
  Coal: "#343232",
  Solar: "#fde511",
  Cogeneration: "#1bcE99",
  Storage: "#dd8738",
  Gas: "#774135",
  Waste: "#be09a1",
  Geothermal: "#04Dc0c",
  "Wave & Tidal": "#0273fe",
  Hydro: "#2c3792",
  Wind: "#08beff",
  Nuclear: "#822378",
  Other: "#c4c4c4",
  Oil: "#cb2140",
};

export const MAP_COLOR_SCALES = {
  irradiation: {
    colours: [
      "#1a1042",
      "#4a1079",
      "#912b81",
      "#d9466b",
      "#fb8560",
      "#fed89a",
      "#fcfdbf",
    ],
    labels: ["≤ 2", "≤ 3", "≤ 4", "≤ 5", "≤ 6", "≤ 7", "≤ 8"],
  },
  windSpeed: {
    colours: ["#e8f1fa", "#c2d9ed", "#7eb9da", "#3989c1", "#08509b", "#08306b"],
    labels: ["≤ 2", "≤ 4", "≤ 6", "≤ 8", "≤ 10", "≥ 10"],
  },
};

export const GRID_COLOR_SCALE = {
  "≤ 10kV": "#7a7a85",
  "≥ 10kV": "#6e97b8",
  "≥ 25kV": "#55b555",
  "≥ 52kV": "#b59f10",
  "≥ 132kV": "#b55d00",
  "≥ 220kV": "#c73030",
  "≥ 330kV": "#b54eb2",
  "≥ 550kV": "#00c1cf",
};
