import { MAPBOX_API_KEY } from "../../config";
import mapboxgl from "mapbox-gl";
import {
  MAP_BASE_INFO,
  GRID_LAYER_INFO,
  getPaintProp,
  powerProjectsLayers,
  MAP_RESOURCE_INFO,
} from "./config";
const BASE_URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/";

export const reverseGeocode = async (longitude, latitude) => {
  const response = await fetch(
    `${BASE_URL}${longitude}, ${latitude}.json?access_token=${MAPBOX_API_KEY}`
  );
  const body = await response.json();
  return body;
};

export const createMarker = (map, name, center, draggable, href, color) => {
  const popupHTML = href
    ? `<h2><a href=${href}> ${name}</a></h2>`
    : `<h2>${name}</h2>`;
  const popup = new mapboxgl.Popup({ closeButton: false })
    .setText(name)
    .setHTML(popupHTML)
    .addTo(map);
  const marker = new mapboxgl.Marker({
    color: color || "#3f587a",
    draggable: draggable,
  })
    .setLngLat(center)
    .addTo(map)
    .setPopup(popup);
  return { marker, popup };
};

export const addLayersToMap = (map) => {
  Object.entries(GRID_LAYER_INFO).forEach(([layer, layerInfo]) => {
    map.addSource(layerInfo.source, {
      type: "vector",
      url: layerInfo.mapboxUrl,
    });

    map.addLayer({
      id: layerInfo.highlightedId,
      source: layerInfo.source,
      "source-layer": layerInfo.sourceLayer,
      type: layerInfo.highlightedShape,
      paint: getPaintProp(layer, layerInfo),
    });
  });
};

export const applyMapEvent = (map, event, handler, mapObjects = []) => {
  if (mapObjects.length) {
    mapObjects.forEach((mapObject) => {
      map.on(event, mapObject, handler);
    });
  } else {
    map.on(event, handler);
  }
};

const getFeatureSources = (feature) => {
  if (Object.values(powerProjectsLayers).includes(feature.layer.id)) {
    return {
      source: GRID_LAYER_INFO.powerProject.source,
      sourceLayer: GRID_LAYER_INFO.powerProject.sourceLayer,
    };
  }
  return {
    source: GRID_LAYER_INFO[feature.layer.id].source,
    sourceLayer: GRID_LAYER_INFO[feature.layer.id].sourceLayer,
  };
};

export const removeFeatureHighlight = (map, feature) => {
  map.removeFeatureState({
    ...getFeatureSources(feature),
    id: feature.id,
  });
};

export const addFeatureHighlight = (map, feature) => {
  const featureState = Object.values(powerProjectsLayers).includes(
    feature.layer.id
  )
    ? {
        id: feature.properties.enian_id,
      }
    : {
        clicked: true,
      };
  map.setFeatureState(
    {
      ...getFeatureSources(feature),
      id: feature.id,
    },
    featureState
  );
};

export const isAFeatureHighlighted = (highlightedFeature) => {
  return !!Object.keys(highlightedFeature).length;
};

export const getFeatureWithBbox = (event, map) => {
  const bbox = [
    [event.point.x - 22, event.point.y - 22],
    [event.point.x + 22, event.point.y + 22],
  ];
  const featuresBbox = map.queryRenderedFeatures(bbox);
  let featuresLayers = [];
  featuresBbox.map((feature) => featuresLayers.push(feature.layer.id));
  const filteredLayer = featuresLayers.filter(
    (layer) => !layer.includes("highlighted")
  )[0];
  const feature = featuresBbox[featuresLayers.indexOf(filteredLayer)];
  return { feature, featuresBbox, filteredLayer };
};

export const checkValidLayerId = (layerId) =>
  Object.keys(GRID_LAYER_INFO)
    .concat(Object.values(powerProjectsLayers))
    .includes(layerId);

export const getLayerPropertyItemProps = (type, value) => {
  switch (type) {
    case "website_location":
      return { field: "Website", valueText: value, isLink: true };

    case "technology_types":
      return {
        field: "Technology types",
        valueText: JSON.parse(value).join(", "),
      };

    case "capacity_kw":
      return { field: "Capacity", valueText: value + "KW" };

    case "dev_status":
      return { field: "Development status", valueText: value };

    default:
      return { field: type, valueText: value };
  }
};

export const lngLatToString = (lngLat) =>
  `(${lngLat.lat.toFixed(4)}, ${lngLat.lng.toFixed(4)})`;

const setLayerVisibilty = (map, layers, visibility) => {
  layers.forEach((layer) => {
    map.setLayoutProperty(layer, "visibility", visibility);
  });
};

const toggleLayer = (map, layerToShow, options) => {
  Object.keys(options).forEach((layer) => {
    const visibility = layer === layerToShow ? "visible" : "none";
    setLayerVisibilty(map, options[layer].layers, visibility);
  });
};

export const toggleBaseLayer = (map, layerToShow) => {
  toggleLayer(map, layerToShow, MAP_BASE_INFO);
};

export const toggleResourceLayer = (map, layerToShow) => {
  toggleLayer(map, layerToShow, MAP_RESOURCE_INFO);
};

export const toggleProjects = (map, show) => {
  setLayerVisibilty(
    map,
    ["power-projects-big", "power-projects-small"],
    show ? "visible" : "none"
  );
};
export const swapLatLng = (coords) => {
  if (!coords) return null;
  return [coords[1], coords[0]];
};

export const drawLine = (map, path) => {
  if (!path) return;
  const swappedPath = path.map((point) => swapLatLng(point));
  map.addSource("route", {
    type: "geojson",
    data: {
      type: "Feature",
      properties: {},
      geometry: {
        type: "LineString",
        coordinates: swappedPath,
      },
    },
  });
  map.addLayer({
    id: "route",
    type: "line",
    source: "route",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#ece",
      "line-width": 8,
    },
  });
};

export const calculateDistance = (start, end) => {
  const toRadian = (n) => (n * Math.PI) / 180;

  const dLon = toRadian(start[0] - end[0]);
  const dLat = toRadian(start[1] - end[1]);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRadian(start[1])) *
      Math.cos(toRadian(end[1])) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  return 6371 * 2 * Math.asin(Math.sqrt(a));
};
