export const snakeToHuman = (string) => string.replaceAll("_", " ");
export const camelToSnakeCase = (str) =>
  str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);
