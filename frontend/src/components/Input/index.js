import cn from "clsx";
import styles from "./Input.module.css";
export const Input = ({
  label,
  name,
  register,
  registerOpts,
  errors,
  className,
  ...props
}) => (
  <div className={cn(styles.wrapper, className)}>
    <label for={name}>{label} </label>
    <input
      name={name}
      className={styles.input}
      {...props}
      {...register(name, registerOpts)}
    />
    {errors?.[name] &&
      (errors[name].type === "required" ? (
        <p className={styles.error}>This field is required</p>
      ) : (
        <p className={styles.error}>
          {" "}
          {errors[name].message || "Please check this input"}
        </p>
      ))}
  </div>
);
