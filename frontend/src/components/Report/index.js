import { minWage } from "../../constants";
import styles from "./Report.module.css";

const readableCoordinates = (coords) => {
  if (!coords) return null;

  return `lat: ${parseFloat(coords[0]).toFixed(4)}, lng: ${parseFloat(
    coords[1]
  ).toFixed(4)}`;
};

const twoDP = (number) =>
  number
    ? new Intl.NumberFormat(undefined, { minimumFractionDigits: 2 }).format(
        number
      )
    : null;
export const Report = ({ project }) => {
  if (!project) return null;
  const construction_cost_per_m = project.overhead
    ? (project.cogs_per_pole + project.labour_per_pole) / project.pole_spacing
    : project.excavation_per_m + project.installation_per_m;
  const totalMultiplier = project.overhead ? 1.3 : 1;
  return (
    <>
      <div className={styles.root}>
        <div className={styles.report}>
          <h1 className={styles.header}>Report</h1>

          <h2>Inputs</h2>
          <div className={styles.section}>
            <h4>Zoom level</h4>
            <p>{project.zoom_level}</p>
            <h4>Capacity (MW)</h4>
            <p>{project.capacity}</p>
            <h4>Voltage (kV)</h4>
            <p>{project.voltage}</p>
            <h4>Plant Location</h4>
            <p>{readableCoordinates(project.start)}</p>
          </div>
          <h2>Outputs</h2>
          <div className={styles.section}>
            <h4>Plant address</h4>
            <p>{project.address}</p>
            <h4>Country code</h4>
            <p>{project.country_code}</p>
            <h4>Substation Location</h4>
            <p>{readableCoordinates(project.end)}</p>
          </div>

          <h3>Cable</h3>
          <div className={styles.section}>
            <h4>Expected current flow (A)</h4>
            <p>{twoDP(project.current)}</p>
            <h4>Expected Cable Cross Section (sq. mm)</h4>
            <p>{twoDP(project.cable_area)}</p>
            <h4>Cable cost per meter ($/m)</h4>
            <p>{twoDP(project.cable_cost_per_m)}</p>
            <h4>Cable length per phase(m)</h4>
            <p>{twoDP(project.cable_dist)}</p>
          </div>

          <h3>Construction</h3>
          <div className={styles.section}>
            <h4>Minimum Wage ($)</h4>
            <p>{minWage[project.country_code]}</p>

            {project.overhead ? (
              <>
                <h4>Material cost ($/pole)</h4>
                <p>{project.cogs_per_pole}</p>
                <h4>Labour ($/pole)</h4>
                <p>{project.labour_per_pole}</p>
                <h4>Pole spacing (m)</h4>
                <p>{project.pole_spacing}</p>
                <h4>Number of poles </h4>
                <p>
                  {Math.ceil(
                    (project.cable_dist * project.terrain_mult) /
                      project.pole_spacing
                  )}
                </p>
              </>
            ) : (
              <>
                <h4>Exacavation cost ($/m)</h4>
                <p>{project.excavation_per_m}</p>
                <h4>Installation cost ($/m)</h4>
                <p>{project.installation_per_m}</p>
              </>
            )}
            <h4>Construction cost ($/m)</h4>
            <p>{twoDP(construction_cost_per_m)}</p>
            <h4>Average terrain multiplier</h4>
            <p>{twoDP(project.terrain_mult)}</p>
            <h4>Effective construction distance</h4>
            <p>{twoDP(project.cable_dist * project.terrain_mult)}</p>
          </div>
          <h3>Subtotal</h3>

          <div className={styles.section}>
            <h4>Subtotal cost of line ($)</h4>
            <p>{twoDP(project.sub_total_cost)}</p>
          </div>

          {project.overhead && (
            <>
              <h3>Overheads</h3>
              <div className={styles.section}>
                <h4>Right of Way (ROW) area (sq. m)</h4>
                <p>{twoDP(project.node_dist * project.cable_dist)}</p>
                <h4>ROW cost [10% of subtotal] ($)</h4>
                <p>{twoDP(0.1 * project.sub_total_cost)}</p>
                <h4>Government license cost [20% of subtotal] ($)</h4>
                <p>{twoDP(0.2 * project.sub_total_cost)}</p>
              </div>
            </>
          )}
          <h3>Total</h3>
          <div className={styles.section}>
            <h4>Total cost of line ($)</h4>
            <p>{twoDP(totalMultiplier * project.sub_total_cost)}</p>
          </div>
        </div>
        <div>
          <h1>Image used for routing calculation</h1>
          <img src={project.static_image} alt="" className={styles.image} />
        </div>
      </div>
    </>
  );
};
