export { Button } from "./Button";

export const KIND = {
  primary: "primary",
  gradient: "gradient",
  inverse: "inverse",
  alt: "alt",
  error: "error",
  textOnly: "textOnly",
};

export const SIZE = {
  large: "large",
};
