import React from "react";
import styles from "./Button.module.css";
import cn from "clsx";
import { KIND, SIZE } from "../index";

export const Button = ({
  children,
  onClick,
  size,
  kind,
  icon,
  className,
  ...rest
}) => {
  return (
    <button
      onClick={onClick}
      className={cn(styles.root, className, {
        [styles.primary]: kind === KIND.primary,
        [styles.gradient]: kind === KIND.gradient,
        [styles.inverse]: kind === KIND.inverse,
        [styles.alt]: kind === KIND.alt,
        [styles.error]: kind === KIND.error,
        [styles.textOnly]: kind === KIND.textOnly,
        [styles.large]: size === SIZE.large,
        [styles.hasIcon]: Boolean(icon),
        [styles.hasNoText]: !children,
      })}
      {...rest}
    >
      {icon && <span className={styles.icon}>{icon}</span>}
      {children}
    </button>
  );
};
