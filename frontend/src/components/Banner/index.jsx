import { useHistory } from "react-router";
import { Logo } from "../Icons";
import { AiOutlineHome } from "react-icons/ai";
import styles from "./Banner.module.css";
const Banner = () => {
  const history = useHistory();
  return (
    <header className={styles.header}>
      <div className={styles.cornerRibbon}>BETA</div>
      <Logo color="#fff" width={145} height={40} />
      <AiOutlineHome
        onClick={() => history.push("/")}
        className={styles.home}
      />
      <h1>Grid Planner</h1>
    </header>
  );
};

export default Banner;
