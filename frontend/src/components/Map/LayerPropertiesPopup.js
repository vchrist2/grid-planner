import { CgClose } from "react-icons/cg";
import { ListItem } from "./ListItem";
import styles from "./LayerPropertiesPopup.module.css";
import { getLayerPropertyItemProps } from "../../services/map";
import { LAYER_TYPES } from "../../services/map/config";

export const LayerPropertiesPopup = ({
  properties,
  coords,
  layerType,
  onCloseButtonClick,
}) => {
  if (!properties) return null;

  return (
    <div className={styles.popupBox}>
      <CgClose onClick={onCloseButtonClick} className={styles.closeButton} />

      <ul>
        <ListItem
          className={styles.prelimInfo}
          key={coords}
          valueText={coords}
        />
        <ListItem
          className={styles.prelimInfo}
          key={layerType}
          field="Type"
          valueText={LAYER_TYPES[layerType]}
        />
        {Object.entries(properties).map(([property, value]) => {
          if (!value || property === "enian_id") return null;
          return (
            <ListItem
              key={property}
              {...getLayerPropertyItemProps(property, value)}
            />
          );
        })}
      </ul>
    </div>
  );
};
