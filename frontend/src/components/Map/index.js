import React, { useState, useEffect, useRef } from "react";
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { mapObjects } from "../../services/map/config";
import { MAPBOX_API_KEY } from "../../config";
import { LayerPropertiesPopup } from "./LayerPropertiesPopup";
import {
  createMarker,
  addLayersToMap,
  applyMapEvent,
  isAFeatureHighlighted,
  getFeatureWithBbox,
  checkValidLayerId,
  removeFeatureHighlight,
  addFeatureHighlight,
  lngLatToString,
  drawLine,
  calculateDistance,
} from "../../services/map";
import styles from "./Map.module.css";

const Map = ({
  start,
  end,
  boundingBox,
  onStartMarkerDragEnd,
  onEndMarkerDragEnd,
  additionalMarkers,
  shortestPath,
  className,
}) => {
  start = start || [-4.23, 55.9];
  end = end || [start[0] + 0.05, start[1] + 0.05];
  mapboxgl.accessToken = MAPBOX_API_KEY;
  const mapContainer = useRef(null);
  const mapRef = useRef(null);
  const startMarkerRef = useRef(null);
  const endMarkerRef = useRef(null);
  const highlightedFeatureRef = useRef({});
  const [layerProperties, setLayerProperties] = useState({
    properties: "",
    coords: "",
    layerType: "",
  });
  const [distance, setDistance] = useState();

  const resetFeatureHighlighting = () => {
    setLayerProperties({
      properties: "",
      coords: "",
      layerType: "",
    });
    removeFeatureHighlight(mapRef.current, highlightedFeatureRef.current);
  };

  const onMapClick = (event) => {
    if (isAFeatureHighlighted(highlightedFeatureRef.current)) {
      resetFeatureHighlighting();
    }
    const map = mapRef.current;
    const { feature, featuresBbox } = getFeatureWithBbox(event, map);
    if (featuresBbox.length && checkValidLayerId(feature.layer.id)) {
      setLayerProperties({
        properties: feature.properties,
        coords: lngLatToString(event.lngLat),
        layerType: feature.layer.id,
      });
      addFeatureHighlight(map, feature);
      highlightedFeatureRef.current = feature;
    }
  };

  const onMarkerDragEndWrapper = () => {
    const { lng, lat } = startMarkerRef.current.getLngLat();
    onStartMarkerDragEnd(lat, lng);
  };

  const onMarkerDragEndWrapper2 = () => {
    const { lng, lat } = endMarkerRef.current.getLngLat();
    onEndMarkerDragEnd(lat, lng);
  };

  useEffect(() => {
    setDistance(calculateDistance(start, end));
  }, [start, end]);

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/keskeny/cksojrlsj19ms18p9vyjym74w",
      zoom: 10,
      center: start,
      bounds: boundingBox,
      logoPosition: "top-right",
    });

    map.on("load", () => {
      const { marker } = createMarker(
        map,
        "Start",
        start,
        true,
        null,
        "#01b4a9"
      );
      const { marker: marker2 } = createMarker(
        map,
        "End",
        end,
        true,
        null,
        "#6f4e89"
      );
      marker.on("dragend", onMarkerDragEndWrapper);
      marker2.on("dragend", onMarkerDragEndWrapper2);
      startMarkerRef.current = marker;
      endMarkerRef.current = marker2;

      additionalMarkers?.forEach((m) => {
        createMarker(map, m.label, m.center, m.draggable, m.href);
      });
      addLayersToMap(map);

      map.addControl(new mapboxgl.FullscreenControl(), "bottom-right");

      drawLine(map, shortestPath);

      const setCursorPointer = () => {
        map.getCanvas().style.cursor = "pointer";
      };
      const resetCursor = () => {
        map.getCanvas().style.cursor = "";
      };
      applyMapEvent(map, "click", onMapClick);
      applyMapEvent(map, "mouseenter", setCursorPointer, mapObjects);
      applyMapEvent(map, "mouseleave", resetCursor, mapObjects);
    });

    mapRef.current = map;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      ref={(el) => (mapContainer.current = el)}
      className={className}
      name="mapWrapper"
    >
      <div className={styles.distance}>{`As the bird flies ${distance?.toFixed(
        2
      )}km`}</div>
      <LayerPropertiesPopup
        onCloseButtonClick={resetFeatureHighlighting}
        {...layerProperties}
      />
    </div>
  );
};

export default Map;
