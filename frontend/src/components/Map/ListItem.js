export const ListItem = ({ field, valueText, className, isLink }) => {
  if (!valueText) return null;
  return (
    <li className={className}>
      <p>
        {field && field + ": "}
        {isLink ? (
          <a href={valueText} target="_blank" rel="noopener noreferrer">
            <b>{valueText}</b>
          </a>
        ) : (
          <b>{valueText}</b>
        )}
      </p>
    </li>
  );
};
