import React from "react";
import styles from "./LoadingPage.module.css";

const LoadingPage = () => {
  return (
    <div className={styles.background}>
      <div className={styles.loader}>Loading</div>
    </div>
  );
};

export default LoadingPage;
