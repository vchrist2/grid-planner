import { snakeToHuman } from "../../utils/stringUtils";
import styles from "./ProjectCard.module.css";
import { useHistory } from "react-router";
export const ProjectCard = ({ project }) => {
  const history = useHistory();
  return (
    <div
      className={styles.card}
      onClick={() => history.push(`/project/${project.id}`)}
    >
      {Object.entries(project)
        .filter(([key, _]) =>
          [
            "id",
            "name",
            "address",
            "country_code",
            "capacity",
            "voltage",
          ].includes(key)
        )
        .map(([key, value]) => (
          <div className={styles.entry} key={key}>
            <p className={styles.cardHeaders}>
              <b>{snakeToHuman(key)}</b>: {value}
            </p>
          </div>
        ))}
    </div>
  );
};
