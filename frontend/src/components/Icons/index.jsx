export const Logo = ({ color = "#041c3d", width = 110, height = 30 }) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 110 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.8748 7.5V0L6.42669 3.73919H6.44811H6.42669L0 7.5L6.42669 11.2392L12.8105 7.54323L12.8748 7.5L12.8105 7.45677L12.8748 7.5Z"
        fill="url(#paint0_linear)"
      />
      <path
        d="M12.8748 22.4992L6.42669 18.76V11.2384L0 7.49921V14.9992V22.4992L6.42669 26.26L12.8748 29.9992V22.4992Z"
        fill="url(#paint1_linear)"
      />
      <path
        d="M25.7493 14.9992V7.49921L19.3226 11.26L12.8745 14.9992L19.3226 18.76L25.7493 14.9992Z"
        fill="url(#paint2_linear)"
      />
      <path
        d="M25.7493 7.5L19.3226 3.73919L12.8745 0V7.5L19.3226 11.2392L25.7493 7.5Z"
        fill="url(#paint3_linear)"
      />
      <path
        d="M94.2151 25.6786H98.8637V16.4495L104.926 25.6786H109.254V7.52299H104.712V17.0979L98.4139 7.52299H94.2151V25.6786Z"
        fill={color}
      />
      <path
        d="M78.5769 25.6786L79.6908 22.5878H86.4174L87.5314 25.6786H92.3514L85.8176 7.52299H80.2906L73.7568 25.6786H78.5769ZM80.7834 18.6973L83.0755 12.0835L85.3677 18.6973H80.7834Z"
        fill={color}
      />
      <path
        d="M44.7514 14.7629H36.5039V11.4127L45.5012 11.3911V7.52224H31.8552V25.6779H45.5012V21.7874H36.5039V18.4372H44.7514V14.7629Z"
        fill={color}
      />
      <path
        d="M48.5857 25.6786H53.2129V16.4495L59.2754 25.6786H63.6241V7.52299H59.0612V17.0979L52.763 7.52299H48.5857V25.6786Z"
        fill={color}
      />
      <path
        d="M71.8716 7.52299H66.9016V25.6786H71.8716V7.52299Z"
        fill={color}
      />
      <defs>
        <linearGradient
          id="paint0_linear"
          x1="4.96804"
          y1="5.98783"
          x2="38.0713"
          y2="3.43512"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0286FE" />
          <stop offset="1" stopColor="#0EFFFF" />
        </linearGradient>
        <linearGradient
          id="paint1_linear"
          x1="1.30532"
          y1="19.9069"
          x2="32.913"
          y2="12.9063"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0261D7" />
          <stop offset="1" stopColor="#0EFFFF" />
        </linearGradient>
        <linearGradient
          id="paint2_linear"
          x1="44.8261"
          y1="12.9235"
          x2="-23.6868"
          y2="13.5479"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0EFFFF" />
          <stop offset="0.131" stopColor="#0DF7FF" />
          <stop offset="0.333" stopColor="#0BE1FF" />
          <stop offset="0.5808" stopColor="#08BEFF" />
          <stop offset="0.8624" stopColor="#048DFE" />
          <stop offset="1" stopColor="#0273FE" />
        </linearGradient>
        <linearGradient
          id="paint3_linear"
          x1="45.5629"
          y1="24.0729"
          x2="-0.198674"
          y2="-4.30631"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0EFFFF" />
          <stop offset="0.1365" stopColor="#0DF7FF" />
          <stop offset="0.3471" stopColor="#0BE1FF" />
          <stop offset="0.6054" stopColor="#08BEFF" />
          <stop offset="0.899" stopColor="#048DFE" />
          <stop offset="1" stopColor="#027BFE" />
        </linearGradient>
      </defs>
    </svg>
  );
};
