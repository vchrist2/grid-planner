import { useState } from "react";
import { useForm } from "react-hook-form";
import cn from "clsx";

import { Input } from "../Input";
import { Button } from "../Buttons";
import { API_ENDPOINT } from "../../config";
import { camelToSnakeCase } from "../../utils/stringUtils";

import styles from "./SearchConfigForm.module.css";

export const SearchConfigForm = ({ project, setProject }) => {
  const { handleSubmit, register, watch } = useForm({
    mode: "onTouched",
    defaultValues: {
      zoomLevel: project?.zoom_level,
      cogsPerPole: project?.cogs_per_pole,
      labourPerPole: project?.labour_per_pole,
      nodeDist: project?.node_dist,
      scaleRatio: project?.scale_ratio,
      overhead: project?.overhead ? "1" : "0",
      excavationPerM: project?.excavation_per_m,
      installationPerM: project?.installation_per_m,
    },
  });
  const [errors, setErrors] = useState();
  const [loading, setLoading] = useState(false);
  const overhead = watch("overhead");

  const onError = (data, e) => {
    setErrors(data);
  };

  const onSubmit = async (data) => {
    setErrors(null);
    setLoading(true);

    const output = {};
    Object.entries(data).forEach(([key, value]) => {
      if (value != null) {
        output[camelToSnakeCase(key)] = value;
      }
    });

    const response = await fetch(`${API_ENDPOINT}project/${project.id}/`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(output),
    });
    const respData = await response.json();
    if (respData?.id) {
      setProject(respData);
    }

    setLoading(false);
  };

  const onSave = handleSubmit(onSubmit, onError);

  return (
    <div className={styles.root}>
      <div className={styles.header}>
        <h2> Search configuration</h2>
        <Button onClick={onSave} disabled={loading}>
          Save
        </Button>
      </div>
      <div className={styles.form}>
        <Input
          label="Zoom level"
          name="zoomLevel"
          type="number"
          register={register}
          registerOpts={{
            placeholder: 13,
            max: {
              value: 13,
              message: "Zoom levels higher than 13 are not currently supported",
            },
            setValueAs: (v) => (v ? parseInt(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="Node dist (m)"
          name="nodeDist"
          type="number"
          register={register}
          registerOpts={{
            min: {
              value: 30,
              message:
                "Node distances lower than 30m are not currently supported",
            },
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />

        <Input
          label="Map scale ratio"
          name="scaleRatio"
          type="number"
          register={register}
          registerOpts={{ setValueAs: (v) => (v ? parseInt(v) : null) }}
          errors={errors}
        />
        <div className={styles.selectWrapper}>
          <label for="overhead">Line type </label>

          <select
            className={styles.select}
            {...register("overhead", { setValueAs: (v) => !!parseInt(v) })}
          >
            <option value="1">Overhead</option>
            <option value="0">Underground</option>
          </select>
        </div>
        <h3>Costs</h3>
        <br />
        <Input
          label="Material cost ($/pole)"
          name="cogsPerPole"
          type="number"
          register={register}
          registerOpts={{ setValueAs: (v) => (v ? parseFloat(v) : null) }}
          errors={errors}
          className={cn({ [styles.hidden]: !overhead })}
        />
        <Input
          label="Labour cost ($/pole)"
          name="labourPerPole"
          type="number"
          register={register}
          registerOpts={{ setValueAs: (v) => (v ? parseFloat(v) : null) }}
          errors={errors}
          className={cn({ [styles.hidden]: !overhead })}
        />

        <Input
          label="Excavation cost ($/m)"
          name="excavationPerM"
          type="number"
          register={register}
          registerOpts={{ setValueAs: (v) => (v ? parseFloat(v) : null) }}
          errors={errors}
          className={cn({ [styles.hidden]: overhead })}
        />
        <Input
          label="Installation cost ($/m)"
          name="installationPerM"
          type="number"
          register={register}
          registerOpts={{ setValueAs: (v) => (v ? parseFloat(v) : null) }}
          errors={errors}
          className={cn({ [styles.hidden]: overhead })}
        />
      </div>
    </div>
  );
};
