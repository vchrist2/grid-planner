import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";

import { Input } from "../../components/Input";
import { Button } from "../../components/Buttons";
import styles from "./BasicProjectForm.module.css";
import { API_ENDPOINT } from "../../config";
export const BasicProjectForm = ({
  project,
  setProject,
  isNew,
  start,
  end,
  location,
}) => {
  const { handleSubmit, register, setValue } = useForm({
    mode: "onTouched",
    defaultValues: {
      name: project?.name,
      address: project?.address,
      countryCode: project?.country_code,
      startLat: project?.start[0],
      startLon: project?.start[1],
      endLat: project?.end[0],
      endLon: project?.end[1],
      capacity: project?.capacity,
      voltage: project?.voltage,
    },
  });
  const [errors, setErrors] = useState();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (start) {
      setValue("startLat", start[0]);
      setValue("startLon", start[1]);
    }
  }, [start, setValue]);

  useEffect(() => {
    if (end) {
      setValue("endLat", end[0]);
      setValue("endLon", end[1]);
    }
  }, [end, setValue]);

  useEffect(() => {
    if (location) {
      setValue("address", location?.features?.[0]?.place_name);
      setValue(
        "countryCode",
        location?.features?.[5]?.properties?.short_code
          ?.toUpperCase()
          .slice(0, 2)
      );
    }
  }, [location, setValue]);

  const onError = (data, e) => {
    setErrors(data);
  };

  const onSubmit = async (data) => {
    setErrors(null);
    setLoading(true);

    const response = await fetch(
      `${API_ENDPOINT}project/${isNew ? "" : project.id.toString() + "/"}`,
      {
        method: isNew ? "POST" : "PATCH", // or 'PUT'
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: data.name,
          address: data.address,
          country_code: data.countryCode,
          capacity: data.capacity,
          voltage: data.voltage,
          start: [data.startLat, data.startLon],
          end: [data.endLat, data.endLon],
        }),
      }
    );
    const respData = await response.json();
    if (respData?.id) {
      if (isNew) {
        history.push(`/project/${respData.id}`);
      } else {
        setProject(respData);
      }
    }

    setLoading(false);
  };

  const onSave = handleSubmit(onSubmit, onError);

  return (
    <div className={styles.root}>
      <div className={styles.header}>
        <h2> Basic details</h2>
        <p>Try moving the markers first</p>
        <Button onClick={onSave} disabled={loading}>
          Save
        </Button>
      </div>
      <div className={styles.form}>
        <Input label="Name" name="name" register={register} errors={errors} />
        <div />
        <Input
          label="Address"
          name="address"
          register={register}
          registerOpts={{ required: true }}
          errors={errors}
        />
        <Input
          label="Country code"
          name="countryCode"
          register={register}
          registerOpts={{ required: true }}
          errors={errors}
        />
        <Input
          label="Start latitude"
          name="startLat"
          register={register}
          registerOpts={{
            required: true,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="Start longitude"
          name="startLon"
          register={register}
          registerOpts={{
            required: true,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="End latitude"
          name="endLat"
          register={register}
          registerOpts={{
            required: true,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="End longitude"
          name="endLon"
          register={register}
          registerOpts={{
            required: true,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="Capacity (MW)"
          name="capacity"
          type="number"
          register={register}
          registerOpts={{
            required: true,
            min: 0,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
        <Input
          label="Voltage (kV)"
          name="voltage"
          type="number"
          register={register}
          registerOpts={{
            required: true,
            min: 0,
            setValueAs: (v) => (v ? parseFloat(v) : null),
          }}
          errors={errors}
        />
      </div>
    </div>
  );
};
