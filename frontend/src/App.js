import React from "react";
import "./App.css";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Home from "./routes/Home";
import Project from "./routes/Project";

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/project/:id" component={Project} />
      <Route>
        <Redirect to="/" />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default App;
