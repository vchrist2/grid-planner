from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers
from grid_planner import views

router = routers.DefaultRouter()
router.register(r"users", views.UserViewSet)
router.register(r"project", views.ProjectViewSet)

urlpatterns = [
    url("nearby_substations", views.NearbySubstations.as_view()),
    url("execute_search", views.ExecuteSearch.as_view()),
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
