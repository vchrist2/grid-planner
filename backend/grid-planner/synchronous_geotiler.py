import logging
from functools import partial
from geotiler.map import fetch_tiles
from geotiler.tile.img import _error_image, _tile_image
from geotiler.tile.io import HEADERS
import requests
import PIL.Image  # type: ignore
import PIL.ImageDraw  # type: ignore

logger = logging.getLogger(__name__)


def render_image(map, tiles):
    if __debug__:
        logger.debug("combining tiles")
    provider = map.provider

    # PIL requires image size to be a tuple
    image = PIL.Image.new("RGBA", tuple(map.size))
    error = _error_image(provider.tile_width, provider.tile_height)

    for tile in tiles:
        img = _tile_image(tile.img) if tile.img else error
        image.paste(img, tile.offset)

    return image


def render_map(map, tiles=None, **kw):
    if not tiles:
        tiles = fetch_tiles(map, sync_fetch_tiles, **kw)

    return render_image(map, tiles)


def sync_fetch_tiles(tiles, num_workers):
    """
    Download map tiles.

    Asynchronous generator of map tiles is returned.

    A collection of tiles is returned. Each successfully downloaded tile
    has `Tile.img` attribute set. If there was an error while downloading
    a tile, then `Tile.img` is set to null and `Tile.error` to a value error.

    :param tiles: Collection of tiles.
    :param num_workers: Number of workers used to connect to a map provider
        service.
    """
    if __debug__:
        logger.debug("fetching tiles...")

        # use `trust_env` to get proxy configuration via env variables
        with requests.Session() as session:
            f = partial(sync_fetch_tile, session)
            tasks = [f(t) for t in tiles]
            for task in tasks:
                tile = task
                yield tile

    if __debug__:
        logger.debug("fetching tiles done")


def sync_fetch_tile(session, tile):
    """
    Fetch map tile.

    :param tile: Map tile.
    """
    try:
        with session.get(tile.url, headers=HEADERS) as response:
            data = response.content
    except Exception as e:
        print(e)
        tile = tile._replace(img=None, error="Error")
    else:
        tile = tile._replace(img=data, error=None)

    return tile
