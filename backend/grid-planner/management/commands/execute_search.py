from django.core.management.base import BaseCommand
from grid_planner.models import Project


class Command(BaseCommand):
    help = "Adds a user to django"

    def add_arguments(self, parser):
        parser.add_argument("id")

    def handle(self, *args, **options):
        p = Project.objects.get(id=int(options["id"]))
        self.stdout.write(f'Found project with id {options["id"]}')
        p.execute()
