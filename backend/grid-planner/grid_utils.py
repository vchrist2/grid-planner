from functools import partial
from matplotlib import pyplot as plt
from django.conf import settings
import io
import itertools
import math
import geotiler
from uuid import uuid4
from operator import attrgetter
from grid_planner.constants import osm_map_color, osm_map_color_ugc
from grid_planner.synchronous_geotiler import render_map
import logging
import tempfile
from pathlib import Path
log = logging.getLogger(__name__)

# The Following Node Datatype is created to store each segment of the map, to perform graph search
class Node:
    def __init__(self, i=0, j=0, f=0, g=0, h=0, coordinates=[0, 0], cost=1.59):
        self.i = i  # each node must know which grid index it is in
        self.j = j  # therefore i and j are required
        self.f = f  # the cost of the cell, f = g + h
        self.g = g  # cost up to current cell
        self.h = h  # heuristic
        self.parent = None  # holds an i, j tuple of grid index

        self.coordinates = coordinates
        self.cost = cost

    def get_neighbours(self, grid):
        for di, dj in itertools.product(range(-1, 2), repeat=2):
            try:
                yield grid[self.i + di][self.j + dj]
            except IndexError:
                continue

    def get_parent(self, grid):
        return grid[self.parent[0]][self.parent[1]] if self.parent else None


class SearchMachineMixin:
    def execute(self):
        map_grid, start_idx, end_idx = self.gen_grid()
        self.save()
        self.a_star_search(map_grid, start_idx, end_idx)
        self.save()
        return self

    def padded_bbox(self):
        """
        Takes 2 coordinates and generates a bbox with some added margin.
        :param start: Starting lat lng Coordinate (plant)
        :param end: Ending lat lng Coordinate (Tower or Substation)
        :param scale_ratio: Ratio of (padded bbox height): (unpadded bbox height)
        :return: padded bounding box, margin, center coordinates
        """
        start = self.start
        end = self.end
        self.center = [(start[0] + end[0]) / 2, (start[1] + end[1]) / 2]  # center coordinates
        center = self.center
        scale_ratio = self.scale_ratio

        vertical_dist_lat = abs(start[0] - end[0])
        horizontal_dist_lon = abs(start[1] - end[1])

        ratio = vertical_dist_lat / horizontal_dist_lon

        if ratio >= 3:
            bot = center[0] - (scale_ratio / 2) * vertical_dist_lat
            L = center[1] - (scale_ratio / 2) * horizontal_dist_lon * ratio
            top = center[0] + (scale_ratio / 2) * vertical_dist_lat
            R = center[1] + (scale_ratio / 2) * horizontal_dist_lon * ratio
            y_margin = (scale_ratio - 1) / 2
            x_margin = y_margin * ratio
            bbox = L, bot, R, top  # Bounding box corners of map image, in coordinates
        elif ratio <= 0.33:
            bot = center[0] - (scale_ratio / 2) * vertical_dist_lat / ratio
            L = center[1] - (scale_ratio / 2) * horizontal_dist_lon
            top = center[0] + (scale_ratio / 2) * vertical_dist_lat / ratio
            R = center[1] + (scale_ratio / 2) * horizontal_dist_lon
            x_margin = (scale_ratio - 1) / 2
            y_margin = x_margin * ratio
            bbox = L, bot, R, top  # Bounding box corners of map image, in coordinates
        else:
            # bounding area of image, in lat lng coordinates adding a padding
            # of 0.5/2 at each border
            bot = center[0] - (scale_ratio / 2) * vertical_dist_lat
            L = center[1] - (scale_ratio / 2) * horizontal_dist_lon
            top = center[0] + (scale_ratio / 2) * vertical_dist_lat
            R = center[1] + (scale_ratio / 2) * horizontal_dist_lon
            y_margin = (scale_ratio - 1) / 2
            x_margin = y_margin
            bbox = L, bot, R, top  # Bounding box corners of map image, in coordinates

        self.margin = (self.scale_ratio - 1) / 2
        self.bbox = bbox  # Bounding box corners of map image, in coordinates

    def gen_map(self, shortest_path=None):
        plt.figure(figsize=(20, 20))
        ax = plt.subplot(111)

        # download background map using OpenStreetMap
        mm = geotiler.Map(extent=self.bbox, zoom=self.zoom_level)
        # img = geotiler.render_map(mm)
        img = render_map(mm)
        ax.imshow(img)
        width, height = img.size
        pixels = list(img.getdata())
        pixels = [pixels[i * width : (i + 1) * width] for i in range(height)]  # noqa E203
        # plot custom points

        x0, y0 = self.start[1], self.start[0]
        x1, y1 = self.end[1], self.end[0]
        points = ((x0, y0), (x1, y1))
        x, y = zip(*(mm.rev_geocode(p) for p in points))
        plt.text(x[0], y[0], "start")
        plt.text(x[1], y[1], "end")
        if shortest_path is None:
            ax.scatter(x, y, c="red", edgecolor="none", s=10, alpha=0.9)
        else:
            for node in shortest_path:
                y, x = node.coordinates
                x, y = mm.rev_geocode((x, y))
                ax.scatter(x, y, c="black", edgecolor="none", s=10, alpha=0.9)

        Path('swallow/images').mkdir(parents=True, exist_ok=True)
        filename = Path(f'map_{self.name}.jpg')
        plt.savefig(Path('swallow/images') / filename)
        plt.close()

        if shortest_path is None:
            return pixels, width, height

    @staticmethod
    def convert_to_index(ratio, dimension):
        return int(round(ratio * dimension))

    def get_corner(self, height_func, width_func):
        return [height_func(self.n_grids_height), width_func(self.n_grids_width)]

    def get_idx(self, coord):
        i = ((coord[0] - self.bbox[3]) / (self.bbox[3] - self.bbox[1])) * self.n_grids_height
        j = ((coord[1] - self.bbox[0]) / (self.bbox[2] - self.bbox[0])) * self.n_grids_width
        return [abs(int(round(i))), abs(int(round(j)))]

    def get_start_and_end(self):
        return self.get_idx(self.start), self.get_idx(self.end)

    def add_to_cell_cost(self, cell_cost, pix_row_idx, pix_col_idx, pix_vals):
        color_dictionary = osm_map_color if self.overhead else osm_map_color_ugc
        try:
            color = pix_vals[pix_row_idx][pix_col_idx]
        except IndexError:
            return
        color = "#%02x%02x%02x" % color[:3]
        # Convert pixel values from RGB to HEX to use in hashmap
        # retrieve pixel cost using pixel colour
        try:
            pix_cost = color_dictionary[color][1]
            cell_cost.append(pix_cost)
        except KeyError:
            return

    def get_cell_cost(self, grid_dist_pix, row, column, pix_vals):
        cell_cost = []  # create an empty list called cell cost
        for row_pix in range(grid_dist_pix):
            for col_pix in range(grid_dist_pix):
                # for every pixel that belongs in the cell, append the pixel cost to the list
                # ignoring unknown colours
                pix_row_idx = row * grid_dist_pix + row_pix
                pix_col_idx = column * grid_dist_pix + col_pix
                self.add_to_cell_cost(cell_cost, pix_row_idx, pix_col_idx, pix_vals)
        return cell_cost

    def gen_grid(self):
        """
        Generates a square grid containing coordinates encompassing start and end.
        :param start: Starting Coordinate (plant)
        :param end: Ending Coordinate (Tower or Substation)
        :param scale_ratio: Ratio of (padded bbox height): (unpadded bbox height)
        :return: 2D list containing coordinates
        """
        log.info("generating grid...")
        self.padded_bbox()

        # The distance in meters of 1 pixel at a given zoom level (e.g 16) as output by geotiler:
        mpix = 40075016.686 * math.cos(math.radians(self.center[0])) / (2 ** (self.zoom_level + 8))
        # 40075016.686 is the circumference of the earth in meters

        # The number of pixels along one axis of each square cell:
        pix_vals, width, height = self.gen_map()
        # node_dist is the spacing between nodes
        grid_dist_pix = int(round(self.node_dist / mpix))

        self.n_grids_width = int(round(width / grid_dist_pix))  # Width of grid in no. of cells
        self.n_grids_height = int(round(height / grid_dist_pix))  # Height of grid in no. of cells
        log.info(f"No of nodes: {self.n_grids_width*self.n_grids_height}")

        lat_step_size = (self.bbox[3] - self.bbox[1]) / self.n_grids_height
        lon_step_size = (self.bbox[2] - self.bbox[0]) / self.n_grids_width

        start_idx, end_idx = self.get_start_and_end()

        grid = [[0] * self.n_grids_width for i in range(self.n_grids_height)]  # create empty grid
        lat = self.bbox[3]  # top most latitude
        for row in range(len(grid)):  # go through the grid
            lon = self.bbox[0]  # left most longitude
            for column in range(len(grid[row])):  # for every cell in the grid
                cell_cost = self.get_cell_cost(grid_dist_pix, row, column, pix_vals)
                if len(cell_cost) == 0:
                    cost = 4
                elif min(cell_cost) <= 1.5:
                    cost = min(cell_cost)
                else:
                    # take the cell cost to be the average of the pixel costs
                    # in that cell
                    #                 cost = sum(cell_cost)/len(cell_cost)
                    cost = max(cell_cost)

                new_node = Node(i=row, j=column, coordinates=[lat, lon], cost=cost)
                # Create a node for that cell with cost and coordinates and place it in the grid
                grid[row][column] = new_node

                lon += lon_step_size

            lat -= lat_step_size

        return grid, start_idx, end_idx

    @staticmethod
    def calculateDistance(x1, y1, x2, y2):
        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    @staticmethod
    def heuristic(x1, y1, x2, y2):
        # This is the octile distance between the node and the end
        # D = 1
        # D2 = math.sqrt(2)
        dx = abs(x1 - x2)
        dy = abs(y1 - y2)

        # return D * (dx + dy) + (D2 - 2 * D) * min(dx, dy)
        return (dx + dy) + (math.sqrt(2) - 2) * min(dx, dy)

    def get_cable_cost_per_m(self):
        # 1.732 is sqrt(3) and 0.8 is the power factor
        current = self.capacity * 1000 / (1.732 * self.voltage)
        if self.overhead:
            area = (current / 22.9) ** (1 / 0.616)  # Cable area with respect to current
        else:
            area = (current / 17.9) ** (1 / 0.617)
        # TODO: This needs a setting for overhead and underground
        cbl_cost_per_m = (171 * current - 988) / 1000  # Cable area to cost
        self.current = current
        self.cable_area = area
        self.cable_cost_per_m = cbl_cost_per_m
        self.save()
        return cbl_cost_per_m, current, area

    # A* search algorithm for least cost path
    def a_star_search(self, grid, start_idx, end_idx):
        log.info("carrying out A* search...")
        log.info(f"k: {self.k}")
        cbl_cost_per_m, _, _ = self.get_cable_cost_per_m()

        if self.overhead:
            construct_cost_per_m = (self.cogs_per_pole + self.labour_per_pole) / self.pole_spacing
        else:
            construct_cost_per_m = self.excavation_per_m + self.installation_per_m

        start = grid[start_idx[0]][start_idx[1]]
        end = grid[end_idx[0]][end_idx[1]]

        open_set = {start}
        closed_set = set()

        while open_set:
            current = min(open_set, key=attrgetter("f"))

            if current == end:
                temp = current
                path = [temp]
                cable_dist = terrain_mult = 0
                while temp.parent:
                    dist = self.calculateDistance(temp.i, temp.j, temp.parent[0], temp.parent[1])
                    cable_dist += dist
                    terrain_mult += dist * temp.cost
                    parent = temp.get_parent(grid)
                    path.append(parent)
                    temp = parent
                self.sub_total_cost = path[0].g
                self.terrain_mult = terrain_mult / cable_dist
                self.cable_dist = self.node_dist * cable_dist
                self.shortest_path = [n.coordinates for n in path]
                self.generate_heatmap(
                    grid, start_idx, end_idx, [n.j for n in path], [n.i for n in path]
                )
                self.save()
                return path

            open_set.remove(current)
            closed_set.add(current)

            for neighbour in current.get_neighbours(grid):
                if neighbour not in closed_set:
                    dist = self.calculateDistance(current.i, current.j, neighbour.i, neighbour.j)
                    tempg = (
                        current.g
                        + (neighbour.cost * construct_cost_per_m + 3 * cbl_cost_per_m)
                        * self.node_dist
                        * dist
                    )
                    # print('neighbour added to closed set')
                    if neighbour in open_set:
                        # print('neihgbour already in open set')
                        if tempg < neighbour.g:
                            neighbour.g = tempg
                            # print('Neighbour g updated')
                    else:
                        neighbour.g = tempg
                        open_set.add(neighbour)
                        # print('Neighbour added to open_set')

                    neighbour.h = self.k * self.heuristic(neighbour.i, neighbour.j, end.i, end.j)
                    neighbour.f = neighbour.g + neighbour.h

                    neighbour_parent = neighbour.get_parent(grid)
                    if (not neighbour_parent) or current.f < neighbour_parent.f:
                        neighbour.parent = (current.i, current.j)

    def generate_heatmap(self, grid, start_idx, end_idx, path_x, path_y):

        heat_data = [[cell.cost for cell in row] for row in grid]
        plt.figure(figsize=(10, 10))

        plt.imshow(heat_data, cmap="RdYlGn_r", interpolation="spline36")
        plt.colorbar()
        plt.text(start_idx[1], start_idx[0], "start", bbox={"fc": "cyan", "alpha": 0.5, "pad": 10})
        plt.text(end_idx[1], end_idx[0], "end", bbox={"fc": "cyan", "alpha": 0.5, "pad": 10})
        plt.plot(path_x, path_y, color="c", linewidth=4)

        Path('swallow/images').mkdir(parents=True, exist_ok=True)
        filename = Path(f'heatmap_{self.name}.jpg')

        plt.savefig(Path('swallow/images') / filename)
        self.static_image = filename.name
        self.save()
