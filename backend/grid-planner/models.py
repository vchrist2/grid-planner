from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import cached_property
import mpu
import math
import logging

from grid_planner.grid_utils import SearchMachineMixin

log = logging.getLogger(__name__)


class Project(SearchMachineMixin, models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    country_code = models.CharField(max_length=2, null=True, blank=True)
    capacity = models.FloatField()
    cable_dist = models.FloatField(null=True)
    current = models.FloatField(null=True)
    voltage = models.FloatField()
    cable_area = models.FloatField(null=True)
    start = models.JSONField(default=list)
    end = models.JSONField(default=list)
    cogs_per_pole = models.FloatField(default=738.45)
    labour_per_pole = models.FloatField(default=1253.55)
    pole_spacing = models.FloatField(default=75.1)
    node_dist = models.IntegerField(default=15)
    zoom_level = models.IntegerField(default=16)
    scale_ratio = models.IntegerField(default=2)
    shortest_path = models.JSONField(default=list)
    static_image = models.TextField(null=True)
    terrain_mult = models.FloatField(null=True)
    sub_total_cost = models.FloatField(null=True)
    cable_cost_per_m = models.FloatField(null=True)
    overhead = models.BooleanField(default=False)
    excavation_per_m = models.FloatField(default=68.5)
    installation_per_m = models.FloatField(default=205.5)

    @cached_property
    def k(self):
        dist = mpu.haversine_distance(self.start, self.end)
        log.info(f"distance: {dist}km")
        return min(29.9115 * math.exp(0.5014 * dist), 8000)

class Minimum_wage(models.Model):
    country_code = models.CharField(max_length=2, null=True, blank=True)
    wages_per_hour = models.FloatField(null=True)

class OSM_color_code(models.Model):
    color_code = models.CharField(max_length=7)
    map_element = models.CharField(max_length=32)
    weight = models.FloatField(null=True)

