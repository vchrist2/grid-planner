from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets, permissions, status
from django.contrib.auth.models import User
from django.conf import settings


from grid_planner.models import Project
from grid_planner.serializers import UserSerializer, ProjectSerializer
from grid_planner.overpass import get_closest_connection_point
import logging

from pathlib import Path
from urllib.parse import urlparse

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class NearbySubstations(APIView):
    def get(self, request, *args, **kwargs):
        lat = float(self.request.query_params.get("lat"))
        lon = float(self.request.query_params.get("lon"))
        radius = self.request.query_params.get("radius") or 5000
        substations, country_code, address = get_closest_connection_point([lat, lon], int(radius))
        return Response(
            data={"country_code": country_code, "address": address, "substations": substations}
        )


class ExecuteSearch(APIView):
    def post(self, request, *args, **kwargs):
        body = self.request.data
        project_id = body.get("project_id")
        project = Project.objects.filter(id=project_id).first()
        if not (project_id and project):
            return Response(
                data="Could not find project with specified ID",
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            project.execute()
        except Exception as e:
            log.error(e)
            return Response(data=str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        project.refresh_from_db()
        serializer = ProjectSerializer(project, context={"request": request})
        return Response(serializer.data)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_images(self, path):
        posix_paths = path.glob("*.jpg")
        return list(posix_paths)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        host_uri = self.request.build_absolute_uri()

        if instance.static_image:
            name = instance.name
            path_list = self.get_images(Path(__file__).resolve().parents[1] / Path('swallow') / Path('images'))

            for idx, path in enumerate(path_list):
                if 'heatmap' in str(path) and name in str(path):
                    instance.static_image = (
                                             f'http://{urlparse(host_uri).netloc}/'
                                             f'images/{path_list[idx].name}'
                                             )
                else:
                    continue

        serializer = self.get_serializer(instance)
        return Response(serializer.data)
