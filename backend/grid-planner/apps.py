from django.apps import AppConfig


class GridPlannerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'grid_planner'
