import mpu
import overpy
from geopy import Nominatim
import logging


def get_closest_connection_point(start, radius=5000):
    api = overpy.Overpass()

    """
    :param start: Lat Lng Coordinates of hypothetical plant
    :return: Coordinates and properties of closest substation using the Overpy API
    """

    locator = Nominatim(user_agent="openstreetmap")
    location = locator.reverse(start)
    country_code = location.raw["address"]["country_code"].upper()

    substations = []
    while not substations:
        logging.info("Searching for substations at radius {}km".format(radius / 1000))
        # Get nodes with tag substation
        response = api.query(
            "node[power=substation](around:{2},{0},{1}); out;".format(start[0], start[1], radius)
        )
        nodes = response.nodes
        for node in nodes:
            substations.append({"lat": float(node.lat), "lon": float(node.lon)})
        # Get ways with tag substation
        response = api.query(
            "way[power=substation](around:{2},{0},{1}); out center;".format(
                start[0], start[1], radius
            )
        )

        for way in response.ways:
            substations.append({"lat": float(way.center_lat), "lon": float(way.center_lon)})

        radius += 5000

        if radius == 50000:
            logging.warning("No results found within radius of 50000")

    if substations:
        for substation in substations:
            dist = mpu.haversine_distance(start, [substation["lat"], substation["lon"]])
            substation["dist"] = dist
        return substations, country_code, location.address
    return None
