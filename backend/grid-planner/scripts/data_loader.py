from grid_planner.constants import osm_map_color_dict, osm_map_color_dict_UGC, min_wage_dict
from grid_planner.models import Minimum_wage, OSM_color_code

def run():
    for key, value in osm_map_color_dict.items():
        OSM_color_code.objects.create(color_code=key, map_element=value[0], weight=value[1])

    for key, value in min_wage_dict.items():
        Minimum_wage.objects.create(country_code=key, wages_per_hour=value)

