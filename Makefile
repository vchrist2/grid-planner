NODE_VERSION := $(shell node --version 2>/dev/null)
PYTHON := ~/.venvs/planner-env/bin/python3

all: requirements deploy create migration migrate npm_install run

requirements:
ifdef NODE_VERSION
	@echo "Found Node version $(NODE_VERSION)"
else
	@echo "Node not found. Please install before running again."
	exit 1
endif

deploy:
	docker-compose -f backend/deploy/docker-compose.yml up -d
create: backend/requirements.txt
	python3 -m venv ~/.venvs/planner-env
	~/.venvs/planner-env/bin/pip install -r backend/requirements.txt
migration:
	$(PYTHON) backend/manage.py makemigrations
migrate:
	$(PYTHON) backend/manage.py migrate
npm_install: frontend/package.json
	cd frontend && npm install
run:
	$(PYTHON) backend/manage.py runserver & cd frontend && npm start

